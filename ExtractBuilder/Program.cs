﻿using ExtractBuilder.QueryBuilder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractBuilder
{
    class Program
    {
        static void Main(string[] args)
        { 
            // Define the variable
            var businessDate = new QueryParameters { Name = "BusinessDate", DataType = "DATE", DefaultValue = "'2019-08-08'" };
            var previousDate = new QueryParameters { Name = "PreviousDate", DataType = "DATE", DefaultValue = "[PMS].[GetPrevBusinessDate](@BusinessDate, 'IBUK')" };
            
            // Define a query used as a input
            var inputTable = new Query(File.ReadAllText("input.sql"), "Transactions");
            inputTable.GetColumns(new[] { businessDate, previousDate });
            
            // Define the processing section of my extract
            var processing = new FunctionBuilder(File.ReadAllText("processing.sql"), "dbo.functionTest");
            
            processing.AddInputQuery(inputTable);
            
            processing.AddParameter(businessDate);
            processing.AddVariable(previousDate);
             
            File.WriteAllText("output_variable.sql", processing.GetExtractQuery());
            File.WriteAllText("output_function.sql", processing.GetExtractFunction());
        }
    }
}
