﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;
using System;
using System.IO;
using System.Linq;

namespace ExtractBuilder.CLI
{
    public class Program
    {
        static IConfiguration config;

        static void PrintHelp()
        {
            Console.WriteLine("extractbuilder.exe [FLAGS] InputScript"); 
            Console.WriteLine("\t-f:FunctionName\t\t|\tCreates a function using the function name");
            Console.WriteLine("\t-q\t\t|\tCreates a executable query");
        }

        static void Startup()
        { 
            config = new ConfigurationBuilder()
              .AddJsonFile("appsettings.json", true, true)
              .Build(); 
        }

        public static void Main(string[] args)
        {
            try
            {
                if (args.Length == 0)
                {
                    PrintHelp();
                    return;
                }
                Startup();

                // Load File
                var flags = args.Where(x => x.Contains('-'));
                var filename = args.Last();

                var script = File.ReadAllText(filename);
                



            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
