

IF OBJECT_ID('dbo.[TestSubscription]', 'U') IS NOT NULL  
	DROP TABLE TestSubscription;
CREATE TABLE [TestSubscription](
   [id] int IDENTITY(1,1),
   [term] nvarchar(128) NULL
);

IF OBJECT_ID('dbo.[Test]', 'U') IS NOT NULL  
	DROP TABLE Test;
CREATE TABLE [Test](
   [id] int IDENTITY(1,1) NOT NULL,
   [domain] nvarchar(128) NULL,
   [title] nvarchar(128) NULL,
   [Subscriptionid] int NULL
);

IF OBJECT_ID('dbo.[Transactions]', 'U') IS NOT NULL  
	DROP TABLE Transactions;
CREATE TABLE [Transactions](
	id INT IDENTITY(1, 1),
	amount DECIMAL(20, 2),
	[description] nvarchar(128),
	[time] DATETIME,
	[account] INT
);

IF OBJECT_ID('dbo.[Accounts]', 'U') IS NOT NULL  
	DROP TABLE Accounts;
CREATE TABLE [Accounts](
	id INT IDENTITY(1, 1),
	[name] varchar(128)
);

IF OBJECT_ID('dbo.[Balances]', 'U') IS NOT NULL  
	DROP TABLE [Balances];
CREATE TABLE [Balances](
	id INT IDENTITY(1, 1),
	[account] INT,
	OpeningBalance DECIMAL(20, 2)
);

INSERT INTO [TestSubscription] (term) VALUES ('Value1');
INSERT INTO [TestSubscription] (term) VALUES ('Value2');
INSERT INTO [TestSubscription] (term) VALUES ('Value3');