﻿	WITH subscriptions AS (
		SELECT * FROM TestSubscription
	), subscriptions_joined AS (
		SELECT a.*, b.id as dup FROM subscriptions a
			LEFT JOIN subscriptions b
				ON a.id = b.id
	)
	SELECT * FROM subscriptions_joined
	WHERE id > 0
	ORDER BY term
