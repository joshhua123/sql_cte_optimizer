
DECLARE @variable0 DATE = '20190201';
DECLARE @variable1 decimal(20,2) = '0.22';
DECLARE @variable2 int SELECT COUNT(1) FROM Accounts;

-- Unsupported
--DECLARE @variable3 DATE, @variable4 varchar = '20190,dsjf211';
--DECLARE @variable5 int SELECT COUNT(1) FROM dual, @variable6 int SELECT COUNT(1) FROM dual;

WITH [values] AS (
	SELECT [date] = @variable0, price = @variable1, qty = @variable2
)

SELECT * FROM [values]