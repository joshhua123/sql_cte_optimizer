﻿DECLARE @Account int = 1;


WITH FilteredTransactions AS (

	SELECT * 
	FROM Transactions
	WHERE account = @Account 

), EnrichedAccounts AS (

	SELECT a.*, b.OpeningBalance
	FROM Accounts a
		LEFT JOIN Balances b 
			ON a.id = b.account

), NewBalances AS (
	SELECT 
		ft.account AS account,
		SUM(ft.amount) + MIN(b.OpeningBalance) AS ClosingBalance
	FROM FilteredTransactions ft
		LEFT JOIN Balances b
			ON ft.account = b.account
	GROUP BY ft.account
)
SELECT * 
FROM EnrichedAccounts ea
INNER JOIN NewBalances nb
	ON ea.id = nb.account