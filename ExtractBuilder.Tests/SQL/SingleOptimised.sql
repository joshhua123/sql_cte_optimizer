﻿DECLARE @BusinessDate DATE = '20191002';
 
WITH [Data] AS (
	SELECT *, @BusinessDate AS BusinessDate FROM Transactions 
)
	SELECT COUNT(1) FROM [Data] WHERE amount > 10			UNION ALL
	SELECT COUNT(1) FROM [Data] WHERE amount > 8			UNION ALL
	SELECT COUNT(1) FROM [Data] WHERE amount > 6			UNION ALL
	SELECT COUNT(1) FROM [Data] WHERE amount > 4			UNION ALL
	SELECT COUNT(1) FROM [Data] WHERE amount > 2			UNION ALL
	SELECT COUNT(1) FROM [Data] WHERE amount >= 0			UNION ALL
	SELECT COUNT(1) FROM [Data] WHERE BusinessDate <> @BusinessDate