﻿using ExtractBuilder.CTEOptimizer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExtractBuilder.Tests
{
    [TestClass]
    public class Graph
    {
        [TestMethod]
        public void Parse()
        {
            foreach (var script in Initialize.SQLScripts.Values)
            {
                var result = CTEParser.Parse(script);
                var graph = DependencyGraph.GenerateGraph(result);

                Assert.IsNotNull(result);
                Assert.IsNotNull(graph);
            }
        }
    }
}
