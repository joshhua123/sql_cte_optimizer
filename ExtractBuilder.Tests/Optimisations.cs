﻿using ExtractBuilder.CTEOptimizer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExtractBuilder.Tests
{
    [TestClass]
    public class Optimisations
    {
        [TestMethod]
        public void UnevenOptimisation()
        {
            var result = CTEParser.Parse(Initialize.SQLScripts["DiamondQuery"]);
            var root = DependencyGraph.GenerateGraph(result);

            Assert.IsNotNull(root);

            var results = new List<SubQuery>(result);
            results.Find(x => x.Identifier == "val1").CacheResult = true;

            var query = QueryGenerator.ConstructOptimisedQuery(root, Initialize.Connection);

            Assert.IsNotNull(query);

            Assert.IsTrue(Initialize.ExecuteCommand(query));
        }
    }
}
