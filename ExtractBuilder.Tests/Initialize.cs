﻿using Docker.DotNet;
using Docker.DotNet.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;  
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExtractBuilder.Tests
{
    [TestClass]
    class Initialize
    { 
        public static SqlConnection Connection;
         
        public static Dictionary<string, string> SQLScripts;

        private static Process dockerProcess;

        public static bool ExecuteCommand(string query)
        {
            using (var command = Connection.CreateCommand())
            {
                command.CommandText = query;
                using (var result = command.ExecuteReader())
                    return result != null;
            }
        }

        public static bool CompareResults(string query1, string query2)
        {
            using (var command1 = Connection.CreateCommand())
            using (var command2 = Connection.CreateCommand())
            {
                command1.CommandText = query1;
                command2.CommandText = query2;

                using (var result1 = command1.ExecuteReader())
                using (var result2 = command2.ExecuteReader())
                {
                    if (!result1.HasRows || !result2.HasRows)
                        return false;

                    result1.Read();
                    result2.Read();

                    for (var i = 0; i < result1.FieldCount; ++i)
                        if (result1.GetDataTypeName(i) != result2.GetDataTypeName(i))
                            return false;

                    return true;
                } 
            }  
        }
         
        private static bool IsDockerRunning()
        {
            dockerProcess = Process.Start(new ProcessStartInfo
            {
                FileName = "docker",
                Arguments = "exec extractbuildertests_sql01_1 ls",
                RedirectStandardOutput = true
            });
            dockerProcess.WaitForExit();
            using (var output = dockerProcess.StandardOutput)
            {
                var line = output.ReadToEnd();
                if(line.Length >= 0)
                        return false;
                return true;
            }
        }

        private static bool StartDocker()
        {
            dockerProcess = Process.Start(new ProcessStartInfo
            {
                FileName = "docker-compose",
                Arguments = "up",
                RedirectStandardOutput = true
            });

            using (var output = dockerProcess.StandardOutput)
            {
                while (!dockerProcess.HasExited)
                {
                    var line = output.ReadLine();
                    if (line.Contains("now ready"))
                        return true;
                    else if (string.IsNullOrWhiteSpace(line))
                        Thread.Sleep(100);
                }
                return false; 
            }
        }
         
        private static void DockerStop()
        {
            dockerProcess = Process.Start(new ProcessStartInfo
            {
                FileName = "docker-compose",
                Arguments = "stop",
                RedirectStandardOutput = true
            });
            dockerProcess.WaitForExit();
        }

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            //if (!IsDockerRunning())
            //    if (!StartDocker())
            //        throw new Exception("Unable to start docker");

            SQLScripts = new Dictionary<string, string>();
            foreach (var file in Directory.GetFiles("SQL"))
                    SQLScripts.Add(Path.GetFileNameWithoutExtension(file), File.ReadAllText(file));
              
            Connection = new SqlConnection("Server=localhost,1234;Database=master;User Id=sa;Password=Password1!;"); 
            Connection.Open();

            var setup = Connection.CreateCommand();
            setup.CommandText = SQLScripts["Initialize"];
            setup.ExecuteNonQuery();

            SQLScripts.Remove("Initialize");  
        }   

        //[AssemblyCleanup]
        //public static void Cleanup()
        //{
        //    Connection.Dispose();
        //    DockerStop(); 
        //}
    }
}
