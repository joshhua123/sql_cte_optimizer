﻿using ExtractBuilder.CTEOptimizer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtractBuilder.Tests
{
    [TestClass]
    public class Query
    {
        [TestMethod]
        public void GetColumns()
        {
            foreach (var script in Initialize.SQLScripts.Values)
            {
                var result = CTEParser.Parse(script);
                var graph = DependencyGraph.GenerateGraph(result);

                Assert.IsNotNull(result);
                Assert.IsFalse(result.Count() == 0);

                foreach (var query in result)
                {
                    query.PopulateColumns(Initialize.Connection);
                    Assert.IsNotNull(query.Columns);
                }
                Assert.IsNotNull(result);
                Assert.IsNotNull(graph);
            } 
        }
    }
}
