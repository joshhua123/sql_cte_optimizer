﻿using ExtractBuilder.CTEOptimizer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtractBuilder.Tests
{
    [TestClass]
    public class Parser
    {  
        [TestMethod]
        public void Parse()
        { 
            foreach (var script in Initialize.SQLScripts.Values)
            {
                var result = CTEParser.Parse(script);
                Assert.IsNotNull(result);
                Assert.IsFalse(result.Count() == 0);
            }
        }
    }
}
