﻿using ExtractBuilder.CTEOptimizer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtractBuilder.Tests
{
    [TestClass]
    public class QueryConstructor
    {
        [TestMethod]
        public void Query()
        { 
            foreach (var script in Initialize.SQLScripts.Values)
            {
                var result = CTEParser.Parse(script);
                var graph = DependencyGraph.GenerateGraph(result);
                var query = QueryGenerator.ConstructOptimisedQuery(graph, Initialize.Connection);

                Assert.IsNotNull(result);
                Assert.IsNotNull(graph);
                Assert.IsNotNull(query);
                Assert.IsFalse(string.IsNullOrWhiteSpace(query));
                Assert.IsTrue(Initialize.ExecuteCommand(query));
                Assert.IsTrue(Initialize.CompareResults(script, query));
            }
        }
          
         
        [TestMethod]
        public void Function()
        {
            foreach (var script in Initialize.SQLScripts.Values)
            {
                var result = CTEParser.Parse(script);
                var graph = DependencyGraph.GenerateGraph(result);
                var query = QueryGenerator.ConstructFunction(graph, Initialize.Connection, graph.Identifier);

                Assert.IsNotNull(result);
                Assert.IsNotNull(graph);
                Assert.IsNotNull(query);
                Assert.IsFalse(string.IsNullOrWhiteSpace(query)); 
            }
        }
    }
}
