﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace ExtractBuilder.CTEOptimizer
{
    public class QueryVariable 
    { 
        public string Identifier { get; set; } 
        public string DataType { get; set; } 
        public string Value { get; set; } 
        // Specifies if a = is required in the definition
        public bool EqualAssertion { get; set; }
    }

    public struct QueryColumn
    {
        public string Name { get; set; }
        public string Datatype { get; set; }
    }

    public class SubQuery
    {
        public string Identifier { get; set; }
        public string Query { get; set; }
        public int StartingIndex { get; set; }
        public int EndingIndex { get => StartingIndex + Length; }
        public int Length { get; set; }
        public List<QueryVariable> Variables;
        public List<SubQuery> Dependents { get; set; }
        public List<SubQuery> Dependencies { get; set; }

        public List<QueryColumn> Columns { get; private set; }

        public bool CacheResult { get; set; }

        public SubQuery()
        {
            Dependencies = new List<SubQuery>();
            Dependents = new List<SubQuery>();
        }

        public IEnumerable<SubQuery> FindInDependencies(Predicate<SubQuery> predicate)
        {
            var matched = new List<SubQuery>();
            var searchStack = new Stack<SubQuery>();
            searchStack.Push(this);

            while (searchStack.TryPop(out var query))
            {
                if (predicate(query))
                    matched.Add(query);

                foreach (var x in query.Dependencies)
                    searchStack.Push(x);
            }

            return matched;
        }

        public void PopulateColumns(DbConnection connection) 
        { 
            if (Columns != null) 
                return; 

            using (var sql_command = connection.CreateCommand())
            {
                sql_command.CommandText =
                    $"exec sp_describe_first_result_set N'{QueryGenerator.ConstructQuery(this).Replace("'", "''")}'";

                using (var result = sql_command.ExecuteReader()) 
                { 
                    while (result.Read())  
                    { 
                        // We do it here to ensure we get a successful read 
                        // before declaring we have column data

                        if (Columns == null) 
                            Columns = new List<QueryColumn>();

                        if (result.IsDBNull(2))
                            continue;
                         
                        Columns.Add( 
                           new QueryColumn 
                           { 
                               Name = result.GetString(2), 
                               Datatype = result.GetString(5) 
                           } 
                       ); 
                    } 
                }
            } 
        }
    }
}
