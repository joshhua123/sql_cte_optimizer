﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace ExtractBuilder.CTEOptimizer
{
    public static class DatabaseMetadata
    {

        private static readonly string[] SIZE_DATA_TYPES = new[] { "varchar", "nvarchar" };
        private static readonly string[] PRECISION_SCALE_TYPES = new[] { "decimal", "numeric" };
        private static readonly Dictionary<Type, string> SystemTypeMap = new Dictionary<Type, string>(); 
        static DatabaseMetadata()
        { 
            SystemTypeMap[typeof(string)] = "nvarchar(4000)";
            SystemTypeMap[typeof(char[])] = "nvarchar(4000)";
            SystemTypeMap[typeof(byte)] = "tinyint";
            SystemTypeMap[typeof(short)] = "smallint";
            SystemTypeMap[typeof(int)] = "int";
            SystemTypeMap[typeof(long)] = "bigint"; 
            SystemTypeMap[typeof(bool)] = "bit";
            SystemTypeMap[typeof(DateTime)] = "datetime";
            SystemTypeMap[typeof(DateTimeOffset)] = "datetimeoffset";
            SystemTypeMap[typeof(decimal)] = "decimal";
            SystemTypeMap[typeof(float)] = "float";
            SystemTypeMap[typeof(double)] = "float";
            SystemTypeMap[typeof(TimeSpan)] = "time"; 
        }

        /// <summary>
        /// Executes the subquery and populates the guery columns based off
        /// the queries result.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="query"></param>
        public static void PopulateColumns(DbConnection connection, SubQuery query)
        {
            query.Columns = new List<QueryColumn>();
            DataTable schema;

            using (var sql_command = connection.CreateCommand())
            {
                sql_command.CommandText = QueryGenerator.ConstructCTE(query, connection);
                using (var result = sql_command.ExecuteReader())
                    schema = result.GetSchemaTable();
            }
             
            foreach (DataRow column in schema.Rows)
            {
                // Query the column for precision information 
                var ColumnName = column["ColumnName"] as string;
                var DataTypeName = column["DataTypeName"] as string;
                var ColumnSize = column["ColumnSize"] as int? ?? -1;

                if (DataTypeName == null)
                    DataTypeName = SystemTypeMap[(column["DataType"] as Type)];

                // Construct the datatype arguments 
                var DataTypeArguments = string.Empty;

                if (PRECISION_SCALE_TYPES.Contains(DataTypeName))
                {
                    var NumericPrecision = column["NumericPrecision"] as short?;
                    var NumericScale = column["NumericScale"] as short?;
                    DataTypeArguments = $"({NumericPrecision}, {NumericScale})";
                }
                else if (SIZE_DATA_TYPES.Contains(DataTypeName))
                    DataTypeArguments = $"({ColumnSize})";

                query.Columns.Add(
                    new QueryColumn
                    {
                        Name = ColumnName,
                        Datatype = DataTypeName + DataTypeArguments
                    }
                );
            }  
        } 
    }
}
