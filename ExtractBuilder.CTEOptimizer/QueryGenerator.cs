﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace ExtractBuilder.CTEOptimizer
{
    public static class QueryGenerator
    {
        /// <summary>
        /// Gets the dependencies of a subquery
        /// 
        /// If includedCached is true then additional queries need to be included to the CTE 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="optimised"></param>
        /// <returns></returns>
        private static Queue<SubQuery> GetDependencies(SubQuery root, bool excludeCached = true)
        {
            var outputStack = new Stack<SubQuery>();
            var processQueue = new Queue<SubQuery>();

            processQueue.Enqueue(root);

            while(processQueue.TryDequeue(out var query))
            {
                foreach (var dependencies in query.Dependencies) {
                    // Don't grab dependencies that are being optimised out
                    if (excludeCached && dependencies.CacheResult)
                        continue;
                     
                    outputStack.Push(dependencies);
                    processQueue.Enqueue(dependencies); 
                }
            }

            var finalOrdering = new Queue<SubQuery>();

            while (outputStack.Count > 0) {
                var query = outputStack.Pop();
                if (!finalOrdering.Contains(query))
                    finalOrdering.Enqueue(query);
            }

            return finalOrdering;
        }
         
        // If we cached a table, we want to update it with a the table variable 
        private static string UpdateTableReferences(SubQuery query, IEnumerable<SubQuery> cachedQueries)
        {
            // Don't transform
            if (cachedQueries.Count() == 0)
                return query.Query;

            var instances = CTEParser.FindReferencedIdentifiers(query.Query)
                .Where(x => cachedQueries.Any(y => y.Identifier.Equals(x.Item1)))
                .ToArray();

            if (instances.Length == 0)
                return query.Query;

            string transformed = query.Query;  
            foreach (var instance in instances) 
                transformed = transformed.Insert(instance.Item2 + 1, "@");

            return transformed;
        }

        private static string ConstructCTE(SubQuery query, bool includeInsert = false)
        {
            var CTEOrder = GetDependencies(query, false);

            var sb = new StringBuilder();
             
            if (CTEOrder.Count != 0)
                sb.Append("WITH ");

            var ExistingIdentifiers = new HashSet<string>();
            while (CTEOrder.TryDequeue(out var subquery))
            {
                // Make sure we don't double declare a query
                if (ExistingIdentifiers.Contains(subquery.Identifier))
                    continue;

                ExistingIdentifiers.Add(subquery.Identifier);

                // CTE Subquery part
                sb.AppendLine($"{subquery.Identifier} AS (");
                sb.AppendLine($"\t{subquery.Query}");
                sb.Append($") {(CTEOrder.Count == 0 ? '\n' : ',')}");
            }

            if (includeInsert)
                sb.AppendLine($"INSERT INTO @{query.Identifier}");

            sb.AppendLine('\t' + query.Query + ';');

            return sb.ToString();
        }

        private static string ConstructOptimisedCTE(SubQuery query, DbConnection connection, bool includeInsert = false)
        {
            var CTEOrder = GetDependencies(query, true);
              
            var sb = new StringBuilder();
              
            // Define any table variables if we're applying optimisations 
            var variableTables = query.FindInDependencies(x => x.CacheResult);
            foreach (var tableVariable in variableTables) 
                sb.AppendLine(ConstructTableVariable(tableVariable, connection));
                        
            if (CTEOrder.Count != 0)
                sb.Append("WITH ");

            var ExistingIdentifiers = new HashSet<string>(); 
            while(CTEOrder.TryDequeue(out var subquery))
            {
                // Make sure we don't double declare a query
                if (ExistingIdentifiers.Contains(subquery.Identifier))
                    continue;
                 
                ExistingIdentifiers.Add(subquery.Identifier);

                // CTE Subquery part
                sb.AppendLine($"{subquery.Identifier} AS ("); 
                sb.AppendLine($"\t{UpdateTableReferences(subquery, variableTables)}"); 
                sb.Append($") {(CTEOrder.Count == 0 ? '\n' : ',')}"); 
            }

            if (includeInsert)
                sb.AppendLine($"INSERT INTO @{query.Identifier}"); 

            sb.AppendLine('\t' + UpdateTableReferences(query, variableTables) + ';');

            return sb.ToString(); 
        }

        public static string ConstructTableVariable(SubQuery query, DbConnection connection)
        {
            var sb = new StringBuilder();
            //DatabaseMetadata.PopulateColumns(connection, query);
            query.PopulateColumns(connection);

            sb.AppendLine($"DECLARE @{query.Identifier} TABLE (");
            foreach (var column in query.Columns)
                sb.AppendLine($"\t[{column.Name}] {column.Datatype}");
            sb.AppendLine(");");

            sb.AppendLine(ConstructOptimisedCTE(query, connection, true));

            return sb.ToString();
        } 

        public static string ConstructQuery(SubQuery root)
        {
            var sb = new StringBuilder();

            sb.AppendLine($"--Function generated on {DateTime.Now.ToString()}"); 
            sb.AppendLine($"--Generator: {Environment.UserName}");             
            foreach (var variable in root.Variables)
                sb.AppendLine($"DECLARE @{variable.Identifier} {variable.DataType} {(variable.EqualAssertion ? '=' : ' ')} {variable.Value};");
              
            sb.AppendLine(ConstructCTE(root, false));

            return sb.ToString();
        }

        public static string ConstructOptimisedQuery(SubQuery root, DbConnection connection)
        {
            var sb = new StringBuilder(); 
            sb.AppendLine($"--Function generated on {DateTime.Now.ToString()}");  
            sb.AppendLine($"--Generator: {Environment.UserName}");              
            foreach (var variable in root.Variables)
                sb.AppendLine($"DECLARE @{variable.Identifier} {variable.DataType} = {variable.Value};");

            // get queries that have been cached and build their table variables
            var cachedSubqueries = root.FindInDependencies(x => !x.CacheResult);  
            foreach (var cached in cachedSubqueries.Where(x => x.Columns == null))
            {
                ///DatabaseMetadata.PopulateColumns(connection, cached);
                cached.PopulateColumns(connection);
                sb.AppendLine(ConstructTableVariable(cached, connection));
            }

            sb.AppendLine(ConstructOptimisedCTE(root, connection));

            return sb.ToString();
        }

        public static string ConstructFunction(SubQuery root, DbConnection connection, string functionName)
        { 
            var sb = new StringBuilder();

///            DatabaseMetadata.PopulateColumns(connection, root);
            root.PopulateColumns(connection);

            sb.AppendLine($"CREATE FUNCTION {functionName} ("); 

            foreach (var variable in root.Variables)
                sb.AppendLine($"\t@{variable.Identifier} {variable.DataType} = {variable.Value}");
              
            sb.AppendLine(")");

            sb.AppendLine($"RETURNS @{root.Identifier} TABLE (");

            foreach (var column in root.Columns)
                sb.AppendLine($"\t[{column.Name}] {column.Datatype}");

            sb.AppendLine(")");
            sb.AppendLine("AS");
            sb.AppendLine("BEGIN");

            // get queries that have been cached and build their table variables
            var cachedSubqueries = root.FindInDependencies(x => x.CacheResult);
            foreach (var cached in cachedSubqueries.Where(x => x.Columns == null)) {
                //DatabaseMetadata.PopulateColumns(connection, cached);
                cached.PopulateColumns(connection);
                sb.AppendLine(ConstructTableVariable(cached, connection));
            }

            sb.AppendLine(ConstructOptimisedCTE(root, connection, true));
            sb.AppendLine("\tRETURN");
            sb.AppendLine("END");

            return sb.ToString();
        }
    }
}  