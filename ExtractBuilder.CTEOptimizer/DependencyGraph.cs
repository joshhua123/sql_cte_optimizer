﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ExtractBuilder.CTEOptimizer
{
    public static class DependencyGraph
    { 
        /// <summary>
        /// Scans the Subquery to find what queries it depends on
        /// </summary>
        /// <param name="queries"></param>
        /// <returns></returns>
        public static SubQuery GenerateGraph(IEnumerable<SubQuery> queries)
        { 
            var stack = new Stack<SubQuery>(16);
            var root = queries.FirstOrDefault(x => x.Identifier == Constants.ROOT_QUERY);
            stack.Push(root);

            while(stack.Count > 0)
            {
                var query = stack.Pop();
                 
                var candidates = CTEParser.FindReferencedIdentifiers(query.Query).Select(x => x.Item1);

                query.Dependencies = queries.Where(x => candidates.Contains(x.Identifier)).ToList();
                foreach (var dependents in query.Dependencies)
                {
                    dependents.Dependents.Add(query);
                    // Don't push for processing if they already have had dependencies checked
                    if(dependents.Dependencies.Count == 0)
                        stack.Push(dependents);
                }
            }

            return root;
        }
    }
}
