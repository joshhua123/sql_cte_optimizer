﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ExtractBuilder.CTEOptimizer
{
    public static class CTEParser
    {
        private static readonly Regex IdentifierGrab = new Regex(@"^\s*(?<Identifier>[\w\[\]]*)\W*", RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly string[] Callers = new string[]{
            "INNER",
            "JOIN",
            "FROM",
            "OUTER"
        };

        private static readonly Regex IsStartCTE = new Regex(@"^\W*[\w\[\]]*\s*AS\s*\(", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);

        private static readonly Regex Variable = new Regex(@"^\s*declare\s*(?<Identifier>[^\s]*)\s*(?<DataType>[^\s]*)\s*=*\s*['\""](?<Value>[^'\""]*)['\""]", RegexOptions.IgnoreCase);
        private static readonly Regex VariableWithPrecision = new Regex(@"^\s*declare\s*(?<Identifier>[^\s]*)\s*(?<DataType>[^\s(]*)\s*(?<DataPrecision>\(*[^)]*)=*\s*['\""](?<Value>[^'\""]*)['\""]", RegexOptions.IgnoreCase);




        // Returns the offset of the next parentheses from the span start
        // First character in the span should be the left paren 
        private static int FindMatchingParenthese(ReadOnlySpan<char> query)
        {
            var depth = 1;
            for (var i = query.IndexOf('(') + 1; i < query.Length; ++i)
            {
                switch (query[i])
                {
                    case '(':
                        depth++;
                        continue;
                    case ')':
                        depth--;
                        continue;
                }
                if (depth == 0)
                    return i;
            }
            throw new Exception($"Unable to find matching parentheses: \n\n{query.ToString()}");
        }

        private static ReadOnlySpan<char> RemoveComment(ReadOnlySpan<char> line)
        {
            var index = line.IndexOf("--");
            if (index == -1)
                return line;
            return line.Slice(0, index);
        }

        /// <summary>
        /// Finds the possible Identifiers of other subqueries within this query. 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IEnumerable<Tuple<string, int>> FindReferencedIdentifiers(string query)
        {
            // Find all potential references
            var candidates = new Stack<Tuple<string, int>>(16);

            foreach (var caller in Callers)
            {
                int offset = 0;
                while ((offset = query.IndexOf(caller, offset, StringComparison.InvariantCultureIgnoreCase) + caller.Length) != caller.Length - 1)
                {
                    // performance boost if we limit the slice   
                    var candidate = IdentifierGrab.Match(query.Substring(offset));

                    if (candidate.Success)
                    {
                        var match = candidate.Groups.FirstOrDefault(x => x.Name == "Identifier")?.Value?.Trim();
                        if (!string.IsNullOrWhiteSpace(match))
                            candidates.Push(new Tuple<string, int>(match, offset));
                    }
                }
            }

            return candidates.ToArray();
        }

        private static IEnumerable<QueryVariable> ParseVariable(ReadOnlySpan<char> line) {

            if (line == null || line.IsWhiteSpace() || !line.Contains("DECLARE", StringComparison.InvariantCultureIgnoreCase))
                return null;
             

            var offset = line.IndexOf("DECLARE", StringComparison.InvariantCultureIgnoreCase) + "DECLARE".Length;

            var IdentifierRegex = new Regex(@"^\s*@(?<Identifier>[^\s]*)\s*(?<Datatype>[^\s(]*)");
             
            var identifier = IdentifierRegex.Match(line.Slice(offset).ToString());

            var dataPrecision = string.Empty;
            var precision = line.Slice(identifier.Length + offset);
            if (precision.Trim().StartsWith("("))
                dataPrecision = precision.Slice(0, precision.IndexOf(')') + 1).ToString();

            // Set Value can either be a query or const

            var defaultValue = ReadOnlySpan<char>.Empty;
            var valueAssertion = false;
            var valueLine = line.Slice(identifier.Length + offset + dataPrecision.Length).Trim();
            if (!valueLine.StartsWith(";"))
            {
                valueAssertion = valueLine.Contains("=", StringComparison.InvariantCultureIgnoreCase);
                valueLine = valueLine.Trim("=");
                defaultValue = valueLine.Slice(0, valueLine.LastIndexOf(';')).Trim();
            }

            return new[]{new QueryVariable
            {
                Identifier = identifier.Groups["Identifier"]?.Value ?? string.Empty,
                DataType = identifier.Groups["Datatype"]?.Value ?? string.Empty + dataPrecision,
                Value = defaultValue.ToString(),
                EqualAssertion = valueAssertion
            }};
        }

        /// <summary>
        /// Extracts an array of SubQueries from @query
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IEnumerable<SubQuery> Parse(string query)
        { 
            var cte_start = query.IndexOf("with", 0, StringComparison.InvariantCultureIgnoreCase) + "with".Length;

            // Process Variables 
            var variables = query
                .Substring(0, cte_start)
                .Split(new string[] { Environment.NewLine, "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries) 
                .Select(x => ParseVariable(RemoveComment(x))) 
                .Where(x => x != null)
                .SelectMany(x => x)
                .ToList(); 

            // Process CTE
            var sections = new Stack<SubQuery>(16);
            do
            {
                var subquery = new SubQuery();
                sections.TryPeek(out var previousSection);
                subquery.StartingIndex = previousSection?.EndingIndex ?? cte_start;
                var line = query.Substring(subquery.StartingIndex);
                if (IsStartCTE.IsMatch(line))
                {
                    subquery.Length =  FindMatchingParenthese(query.AsSpan(subquery.StartingIndex));

                    var index_of_as = query.IndexOf("AS", subquery.StartingIndex, StringComparison.InvariantCultureIgnoreCase);
                    var post_as = index_of_as + "AS".Length;

                    subquery.Identifier = query
                        .AsSpan(subquery.StartingIndex, index_of_as - subquery.StartingIndex)
                        .Trim()
                        .Trim(new char[] { ',', '(', ' ' })
                        .Trim()
                        .ToString();

                    subquery.Query = query
                        .AsSpan(post_as, subquery.EndingIndex - post_as) 
                        .Trim()
                        .Trim(new char[] { ',', '(', ')' })
                        .Trim()
                        .ToString();
                }
                else // The last node
                {
                    subquery.Identifier = Constants.ROOT_QUERY;
                    subquery.Length = query.Length - subquery.StartingIndex;

                    subquery.Query = query
                        .AsSpan(subquery.StartingIndex, subquery.Length)
                        .Trim()
                        .Trim(new char[] { ',', '(', ')' })
                        .ToString(); 
                }

                // Add variables to the subquery
                subquery.Variables = variables;

                sections.Push(subquery);
            }
            while (sections.Peek().Identifier != Constants.ROOT_QUERY);
             

            return sections.ToArray();
        }
    }
}
