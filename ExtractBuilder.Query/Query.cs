﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ExtractBuilder.QueryBuilder
{
    public class Query
    {
        public Query(DbConnection connection, string command, string identifier)
        {
            Command = command;
            Identifier = identifier;
            IsCached = true;
            this.connection = connection;
        }
        public string Identifier { get; set; }
        public string Command { get; set; }
        public List<QueryColumn> Columns { get; set; }
        public bool IsCached { get; set; }

        private DbConnection connection;

        public bool GetColumns(IEnumerable<QueryParameters> parameters = null)
        {
            if (Columns != null)
                return true;

            Columns = new List<QueryColumn>();

            try
            {
                // Format the parameters
                var definitions = parameters
                        ?.Select(p => p.ToStringAsVariable())
                        ?.Aggregate((var1, var2) => var1 + Environment.NewLine + var2) ?? string.Empty;


                using (var sql_command = connection.CreateCommand())
                {
                    sql_command.CommandText = definitions + Command;
                    using (var result = sql_command.ExecuteReader())
                    { 
                        var resultSchema = result.GetSchemaTable();

                        var sizeDataTypes = new[] { "varchar", "nvarchar" };
                        var precisionScaleDataTypes = new[] { "decimal", "numeric" };

                        foreach (DataRow column in resultSchema.Rows)
                        {
                            // Query the column for precision information 
                            var ColumnName = column["ColumnName"] as string;
                            var DataTypeName = column["DataTypeName"] as string;
                            var ColumnSize = column["ColumnSize"] as int? ?? -1;

                            // Construct the datatype arguments 
                            var DataTypeArguments = string.Empty;

                            if (precisionScaleDataTypes.Contains(DataTypeName))
                            {
                                var NumericPrecision = column["NumericPrecision"] as short?;
                                var NumericScale = column["NumericScale"] as short?;
                                DataTypeArguments = $"({NumericPrecision}, {NumericScale})";
                            }
                            else if (precisionScaleDataTypes.Contains(DataTypeName))
                                DataTypeArguments = $"({ColumnSize})";


                            Columns.Add(
                                new QueryColumn
                                {
                                    Name = ColumnName,
                                    DataType = DataTypeName + DataTypeArguments
                                }
                            );
                        }

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            return false;
        }

        public string ToStringArguments()
        {
            if ((Columns?.Count ?? 0) == 0)
                throw new Exception("Column data required, execute GetColumns first");

            return
                Columns
                    .Select(col => $"\t[{col.Name}] {col.DataType}")
                    .Aggregate((l1, l2) => $"{l1}, {Environment.NewLine}{l2}");
        }

        public string ToStringAsInsertInto(string into)
        {
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO @{into}");
            sb.AppendLine(Command.Insert(0, "\t").Replace(Environment.NewLine, $"{Environment.NewLine}\t"));
            sb.Append(';');
            return sb.ToString();
        }

        public string ToStringAsTableVariable()
        {
            if ((Columns?.Count ?? 0) == 0)
                throw new Exception("Column data required, execute GetColumns first");

            var sb = new StringBuilder();

            // Declare the table variable
            sb.AppendLine($"DECLARE @{Identifier} TABLE (");
            sb.AppendLine(ToStringArguments());
            sb.AppendLine(")");

            // Insert data into the variable

            sb.AppendLine(ToStringAsInsertInto(Identifier));

            return sb.ToString();
        }

        public string ToStringAsSubquery()
        {
            if ((Columns?.Count ?? 0) == 0)
                throw new Exception("Column data required, execute GetColumns first");

            var sb = new StringBuilder();

            sb.AppendLine($"{Identifier} AS (");
            sb.AppendLine(Command.Insert(0, "\t").Replace(Environment.NewLine, $"{Environment.NewLine}\t"));
            sb.AppendLine($")");

            return sb.ToString();
        }
    }
}
