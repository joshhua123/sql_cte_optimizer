﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtractBuilder.QueryBuilder
{
    public class FunctionBuilder
    {
        List<QueryParameters> parameters = new List<QueryParameters>();
        List<QueryParameters> variables = new List<QueryParameters>();
        // Input queries need to be named so we can refer to them within the provided processing script and fix up any references.
        List<Query> inputQueries = new List<Query>();

        private string processingCommand;
        private string functionName;

        public FunctionBuilder(string command, string functionName)
        {
            processingCommand = command;
            this.functionName = functionName;
        } 
          
        public void AddParameter(QueryParameters parameter) => parameters.Add(parameter);
        public void AddInputQuery(Query input) => inputQueries.Add(input); 
        public void AddVariable(QueryParameters variable) => variables.Add(variable);
         
        // Get the return types for the the processing query 
        private Query GetProcessingQuery()
        { 
            StringBuilder CommandBuilder = new StringBuilder(processingCommand);

            // Correct the reference to the input query within processing
            foreach(var query in inputQueries)
            {
                var matchLength = $"^{query.Identifier}".Length;
                var matches = new List<int>();
                var offset = 0;
                var match = 0;

                // Find all occurances of this identifier
                while((match = processingCommand.IndexOf($"^{query.Identifier}", offset)) != -1) {
                    matches.Add(match);
                    offset = match + matchLength;
                }

                // Perform a character replace to refer to a table variable or CTE 
                foreach (var occurance in matches)
                    CommandBuilder[occurance] = (query.IsCached) ? '@' : ' ';
            }

            string Command = CommandBuilder.ToString();

            // Build the query

            // We need to handle input queries that have a CTE and command queries with CTE
              
            var commandQuery = new Query(
                null,
                inputQueries
                    .Select(x => x.ToStringAsTableVariable())
                    .Append(Command.ToString()) // Add the command after the input variables
                    .Aggregate((var1, var2) => $"{var1}{Environment.NewLine}{var2}")
                , "__Export"
                );

            commandQuery.IsCached = false;

            // Merge the parameters and variables 
            var commandParameters = parameters.ToList();
                commandParameters.AddRange(variables);
                
            var success = commandQuery.GetColumns(commandParameters);
            return commandQuery;
        }


        public string GetExtractQuery()
        {
            try
            {
                var sb = new StringBuilder();

                var processingQuery = GetProcessingQuery();

                // Define variables

                sb.AppendLine(parameters.Select(x => x.ToStringAsVariable()).Aggregate((x, y) => x + Environment.NewLine + y));

                sb.AppendLine(variables.Select(x => x.ToStringAsVariable()).Aggregate((x, y) => x + Environment.NewLine + y));

                sb.AppendLine(processingQuery.Command);

                return sb.ToString();

                // Insert command as insert statement into return table
                  
                // return result

            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace); 
                return ex.Message;
            }
        }

        public string GetExtractFunction()
        {
            try
            {
                var sb = new StringBuilder();

                var processingQuery = GetProcessingQuery();

                // Create function definition

                sb.AppendLine($"CREATE FUNCTION {functionName} (");
                sb.AppendLine(parameters.Select(x => $"\t{x.ToStringAsParameter()}").Aggregate((x, y) => $"{x},\n{y}"));
                sb.AppendLine(")");

                // Define Return Table

                sb.AppendLine($"RETURNS @{processingQuery.Identifier} TABLE (");
                sb.AppendLine(processingQuery.ToStringArguments());
                sb.AppendLine(")");
                 
                sb.AppendLine("AS");
                sb.AppendLine("BEGIN");

                // Define variables

                foreach (var variable in variables)
                    sb.AppendLine(variable.ToStringAsVariable());

                // Insert input commands

                foreach (var query in inputQueries)
                    sb.AppendLine(query.ToStringAsTableVariable());

                // Insert command as insert statement into return table

                sb.AppendLine(processingQuery.ToStringAsInsertInto(processingQuery.Identifier));

                // return result
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return ex.Message;
            }
        }

    }
}
