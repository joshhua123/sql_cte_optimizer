﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtractBuilder.QueryBuilder
{
    public class QueryColumn
    { 
        public string Name { get; set; }
        public string DataType { get; set; } 
    }
}
