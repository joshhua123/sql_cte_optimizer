﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtractBuilder.QueryBuilder
{
    public class QueryParameters
    { 
        public string Name { get; set; }
        public string DataType { get; set; }
        public string DefaultValue { get; set; }

        public string ToStringAsVariable() => $"DECLARE @{Name} {DataType} = {DefaultValue};{Environment.NewLine}";

        public string ToStringAsParameter() => $"@{Name} {DataType}";
    }
}
